
from matplotlib.pyplot import *
import linecache
import math

f1 = open("./Figure2_CHEM_N.out","r")


line_num = 0


pass_line=390*20
for w in range(pass_line):
    result_f1=f1.readline()


for part in range(1):

    #Production of N+
    photoN = np.zeros(390)
    k15 = np.zeros(390)
    k9 = np.zeros(390)

    #Loss of N+
    k5 = np.zeros(390)
    k7 = np.zeros(390)
    k6 = np.zeros(390)
    k8 = np.zeros(390)
    k17 = np.zeros(390)
    k18 = np.zeros(390)
    r3 = np.zeros(390)


    index_x = np.linspace(2.2E2, 80.2E2, num=390)

    for j in range(390):
        result_f1 = f1.readline().split()

        for i in range(14):
            if(i > 3):
                result_f1[i] = float(result_f1[i])

        #put number in array (By reaction)
        photoN[j]=result_f1[4]
        k15[j]=result_f1[5]
        k9[j]=result_f1[6]

        k5[j]=result_f1[7]
        k7[j]=result_f1[8]
        k6[j]=result_f1[9]
        k8[j]=result_f1[10]
        k17[j]=result_f1[11]
        k18[j]=result_f1[12]
        r3[j]=result_f1[13]

#Plot N+ production
figure(figsize=(6,6),dpi=300)
semilogx(photoN[0:100],index_x[0:100],'C1-',k15[0:100],index_x[0:100],'C1--',k9[:100],index_x[:100],'C1-.')

legend((r'SE Production',r'$He^+$ + $N_2$',r'$N_2^+$ + $N$'),loc='best',prop={'size': 10})
xlabel(r'Production Rate(cm$^{-3}$$s^{-1}$)',fontsize=14)
ylabel("Altitude(km)",fontsize=14)
yticks(fontsize=14)
xticks(fontsize=14)
axis([10**(-14), 10**3, 0, 2500])
tight_layout()

fname = "CHEMN_Production.eps"
savefig(fname,format='eps')

#Plot Loss
figure(figsize=(6,6),dpi=300)
semilogx(k5[0:100],index_x[0:100],'C1-',k7[0:100],index_x[0:100],'C1--',k6[:100],index_x[:100],'C1-.')
semilogx(k8[0:100],index_x[0:100],'C1:',k17[0:100],index_x[0:100],'C1-x',
        k18[0:100],index_x[0:100],'C1-+',r3[0:100],index_x[0:100],'C1-1')

legend((r'$N^+$ + $O_2$ (1)',r'$N^+$ + $O$',r'$N^+$ + $NO$',
        r'$N^+$ + $H$',r'$N^+$ + $O_2$ (2)',r'$N^+$ + $O_2$ (3)',r'$N^+$ + $e^-$'),loc='best',prop={'size': 10})
xlabel(r'Loss Rate(cm$^{-3}$$s^{-1}$)',fontsize=14)
ylabel("Altitude(km)",fontsize=14)
yticks(fontsize=14)
xticks(fontsize=14)
axis([10**(-14), 10**3, 0, 2500])
tight_layout()

fname = "CHEMN_Loss.eps"
savefig(fname,format='eps')

f1.close()
#f2.close()
