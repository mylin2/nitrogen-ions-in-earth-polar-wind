
from matplotlib.pyplot import *
import linecache
import math

f1 = open("./Figure2_CHEM_H.out","r")
old_f1 = open("./Figure2_3iPWOM_ion.out","r")
#get neutral density
old_f2 = open("./Figure2_3iPWOM_neutral.out","r")


line_num = 0


pass_line=390*20
for w in range(pass_line):
    result_f1=f1.readline()

for w in range(5):
    result_old_f1=old_f1.readline()
    result_old_f2=old_f2.readline()


for part in range(1):

    #Production of H+
    photoH = np.zeros(390)
    k4 = np.zeros(390)
    k8 = np.zeros(390)

    #Loss of O+
    k13 = np.zeros(390)
    r1 = np.zeros(390)

    k13_old=np.zeros(390)#loss H+
    k4_old=np.zeros(390)#produce H+

    index_x = np.linspace(2.2E2, 80.2E2, num=390)

    for j in range(390):
        result_f1 = f1.readline().split()
        result_old_f1=old_f1.readline().split()
        result_old_f2=old_f2.readline().split()

        for i in range(9):
            if(i > 3):
                result_f1[i] = float(result_f1[i])

        #old pwom
        old_k13=2.2E-11*float(result_old_f1[14])**0.5
        old_k4=2.5E-11*float(result_old_f1[14])**0.5
        k13_old[j]=float(result_old_f1[8])*float(result_old_f2[3])*old_k13
        k4_old[j]=float(result_old_f1[7])*float(result_old_f2[6])*old_k4
        #put number in array (By reaction)
        photoH[j]=result_f1[4]
        k4[j]=result_f1[5]
        k8[j]=result_f1[6]

        k13[j]=result_f1[7]
        r1[j]=result_f1[8]

figure(figsize=(6,6),dpi=300)
semilogx(photoH[0:100],index_x[0:100],'C8-',k4[0:100],index_x[0:100],'C8--',k8[:100],index_x[:100],'C8-.')
semilogx(k4_old[0:100],index_x[0:100],'k--')

legend((r'SE Production',r'$O^+$ + $H$',r'$N^+$ + $H$'),loc='best',prop={'size': 10})
xlabel(r'Production Rate(cm$^{-3}$$s^{-1}$)',fontsize=14)
ylabel("Altitude(km)",fontsize=14)
yticks(fontsize=14)
xticks(fontsize=14)
axis([10**(-7), 10**1, 0, 2500])
tight_layout()

fname = "CHEMH_Production.eps"
savefig(fname,format='eps')

#Plot Loss
figure(figsize=(6,6),dpi=300)
semilogx(k13[0:100],index_x[0:100],'C8-',r1[0:100],index_x[0:100],'C8-1')
semilogx(k13_old[0:100],index_x[0:100],'k-')

legend((r'$H^+$ + $O$',r'$H^+$ + $e^-$'),loc='best',prop={'size': 10})
xlabel(r'Loss Rate(cm$^{-3}$$s^{-1}$)',fontsize=14)
ylabel("Altitude(km)",fontsize=14)
yticks(fontsize=14)
xticks(fontsize=14)
axis([10**(-7), 10**1, 0, 2500])
tight_layout()

fname = "CHEMH_Loss.eps"
savefig(fname,format='eps')

f1.close()
