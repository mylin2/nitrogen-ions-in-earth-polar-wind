
from matplotlib.pyplot import *
import linecache
import math

f1 = open("./Figure3_OGOSD.out","r")
f2 = open("./Figure3_old_OGOSD.out","r")

line_num = 5

for part in range(1):
    figure(figsize=(3.5,5),dpi=300)

    for w in range(line_num):
	    result_f2=f2.readline()
	    result_f1=f1.readline()

    index_x = np.zeros(397-line_num)
    index_y = np.zeros(397-line_num)

    index_x_2 = np.zeros(397-line_num)
    index_y_2 = np.zeros(397-line_num)

    index_x_3 = np.zeros(397-line_num)
    no_nitro_O = np.zeros(397-line_num)
    no_nitro_H = np.zeros(397-line_num)
    no_nitro_He = np.zeros(397-line_num)
    no_nitro_e = np.zeros(397-line_num)

    nitro_O=np.zeros(397-line_num)
    nitro_H=np.zeros(397-line_num)
    nitro_He = np.zeros(397-line_num)
    nitro_N=np.zeros(397-line_num)
    nitro_N2=np.zeros(397-line_num)
    nitro_e=np.zeros(397-line_num)
    nitro_NO=np.zeros(397-line_num)
    nitro_O2=np.zeros(397-line_num)

    for j in range(397-line_num):
        result_f1 = f1.readline().split()
        result_f2 = f2.readline().split()

        for i in range(26):
            result_f1[i] = float(result_f1[i])
            if (i==0):
                index_x[j]=result_f1[i]
        for i in range(21):
            result_f2[i]=float(result_f2[i])
        nitro_O[j]=result_f1[19]
        nitro_H[j]=result_f1[20]
        nitro_He[j]=result_f1[21]
        nitro_N[j]=result_f1[22]
        nitro_N2[j]=result_f1[23]
        nitro_NO[j]=result_f1[24]
        nitro_O2[j]=result_f1[25]
        nitro_e[j]=result_f1[26]
        no_nitro_O[j]=result_f2[11]
        no_nitro_H[j]=result_f2[12]
        no_nitro_He[j]=result_f2[13]
        no_nitro_e[j]=result_f2[14]
    plot(nitro_O[:390],index_x[:390],'b-',nitro_N[:390],index_x[:390],'C1-',nitro_H[:390],index_x[:390],'C8-',linewidth=3)
    plot(nitro_He[:390],index_x[:390],'g-',nitro_e[:390],index_x[:390],'k-',linewidth=3)#,nitro_N2[:390],index_x[:390], 'r-',nitro_NO[:390],index_x[:390],'#ff81c0')
    plot(no_nitro_O[:390],index_x[:390],'b--',no_nitro_H[:390],index_x[:390],'C8--', no_nitro_He[:390],index_x[:390],'g--',linewidth=3)
    plot(no_nitro_e[:390],index_x[:390],'k--',linewidth=3)
    xlabel("Temperature(K)")
    fname = "Figure3a"+'.eps'
    axis([800,4000,0,8400])
    tick_params(axis='x',labelsize=14)
    tight_layout()
    savefig(fname,format='eps')
f1.close()
f2.close()
