# Nitrogen Ions in Earth Polar Wind

The python codes and data here can be regenerated the figures of the paper under review in Geophysical Research Letters. Download all the files. For Figure1, Figure1.py can be regenerated. For Figure2 and Figure3, execute ./figure2 and ./figure3 respectively. Each of the figure generated in Figure1 and Figure2 has been matched with one of the picture in figure1 and figure2. All of the codes can be compiled for python 3.7.7.
