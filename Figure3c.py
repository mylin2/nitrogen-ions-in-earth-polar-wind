from matplotlib.pyplot import *
import linecache
import math


f1_s = open ("./Figure3_ProductionN_SolarMax.out","r")
f2_s = open ("./Figure3_ProductionN_SolarMin.out","r")


for part in range(1):

    #Production of N+
    photoN_SolarMax = np.zeros(390)
    photoN_SolarMin = np.zeros(390)

    k15_SolarMax = np.zeros(390)
    k9_SolarMax = np.zeros(390)

    k15_SolarMin = np.zeros(390)
    k9_SolarMin = np.zeros(390)

    for j in range(390):
        SolarMax_SE = f1_s.readline().split()
        photoN_SolarMax[j] = float(SolarMax_SE[1])
        k15_SolarMax[j]=float(SolarMax_SE[2])
        k9_SolarMax[j]=float(SolarMax_SE[3])

        SolarMin_SE = f2_s.readline().split()
        photoN_SolarMin[j] = float(SolarMin_SE[1])
        k15_SolarMin[j]=float(SolarMin_SE[2])
        k9_SolarMin[j]=float(SolarMin_SE[3])


index_x = np.linspace(2.2E2, 80.2E2, num=390)

figure(figsize=(3.5,5),dpi=300)
semilogx(photoN_SolarMax[0:100],index_x[0:100],'C1-',photoN_SolarMin[0:100],index_x[0:100],'C1-.',
        k15_SolarMax[0:100],index_x[0:100],'C4-',k15_SolarMin[0:100],index_x[0:100],'C4-.',
        k9_SolarMax[0:100],index_x[0:100],'r-',k9_SolarMin[0:100],index_x[0:100],'r-.',
        markevery=5,linewidth=3,markersize=10)

xlabel(r'Production Rate(cm$^{-3}$$s^{-1}$)',fontsize=14)
ylabel("Altitude(km)",fontsize=14)
yticks(fontsize=14)
xticks((10**(-12), 10**(-8),10**(-4),10**(0), 10**2), (r'10$^{-12}$', r'10$^{-8}$',r'10$^{-4}$', r'10$^{0}$',r'10$^{2}$'), color='k', size=10)
#xticks(fontsize=14)
axis([10**(-14), 10**3, 0, 2500])
tight_layout()

fname = "Figure3c.eps"
savefig(fname,format='eps')

f1_s.close()
f2_s.close()
