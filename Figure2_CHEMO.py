
from matplotlib.pyplot import *
import linecache
import math


f1 = open("./Figure2_CHEM_O.out","r")

old_f1 = open("./Figure2_3iPWOM_ion.out","r")
#get neutral density
old_f2 = open("./Figure2_3iPWOM_neutral.out","r")
#get production Rate
old_f3 = open("./Figure2_Oproduction.out","r")
#import chemical reaction rate
old_k0=1.2E-12
old_k1=2.1E-11
old_k2=9.7E-10
old_k3=5.2E-10
old_k15=7.8E-10


line_num = 0


pass_line=390*0
for w in range(pass_line):
    result_f1=f1.readline()

for w in range(5):
    result_old_f1=old_f1.readline()
    result_old_f2=old_f2.readline()


for part in range(1):

    #Production of O+
    photoO = np.zeros(390)
    k2 = np.zeros(390)
    k13 = np.zeros(390)
    k18 = np.zeros(390)
    k7 = np.zeros(390)
    k16 = np.zeros(390)

    #Loss of O+
    k0 = np.zeros(390)
    k1 = np.zeros(390)
    k4 = np.zeros(390)
    k14 = np.zeros(390)
    r0 = np.zeros(390)

    #production of O+ in 3iPWOM
    k2_old=np.zeros(390)
    k13_old=np.zeros(390)
    photoO_old=np.zeros(390)

    #loss of O+ in 3iPWOM
    k0_old=np.zeros(390)
    k1_old=np.zeros(390)
    k4_old=np.zeros(390)




    index_x = np.linspace(2.2E2, 80.2E2, num=390)

    for j in range(390):
        result_f1 = f1.readline().split()
        result_old_f1=old_f1.readline().split()
        result_old_f2=old_f2.readline().split()
        result_old_f3=old_f3.readline().split()

        for i in range(21):
            if(i > 9):
                result_f1[i] = float(result_f1[i])

        #old pwom production and loss
        old_k13=2.2E-11*float(result_old_f1[14])**0.5
        old_k4=2.5E-11*float(result_old_f1[14])**0.5
        photoO_old[j]=float(result_old_f3[1])
        k2_old[j]=float(result_old_f1[9])*float(result_old_f2[4])*old_k2
        k13_old[j]=float(result_old_f1[8])*float(result_old_f2[3])*old_k13

        k0_old[j]=float(result_old_f1[7])*float(result_old_f2[5])*old_k0
        k1_old[j]=float(result_old_f1[7])*float(result_old_f2[4])*old_k1
        k4_old[j]=float(result_old_f1[7])*float(result_old_f2[6])*old_k4

        #put number in array (By reaction)
        photoO[j]=result_f1[10]
        k2[j]=result_f1[11]
        k13[j]=result_f1[12]
        k18[j]=result_f1[13]
        k7[j]=result_f1[14]
        k16[j]=result_f1[15]

        k0[j]=result_f1[16]
        k1[j]=result_f1[17]
        k4[j]=result_f1[18]
        k14[j]=result_f1[19]
        r0[j]=result_f1[20]



#Plot O+ production
figure(figsize=(6,6),dpi=300)
semilogx(photoO[0:100],index_x[0:100],'b-',k2[0:100],index_x[0:100],'b--',k13[:100],index_x[:100],'b-.')
semilogx(k18[0:100],index_x[0:100],'b:',k7[0:100],index_x[0:100],'b-1',k16[:100],index_x[:100],'b-*')
semilogx(photoO_old[0:40],index_x[0:40],'k-',k2_old[0:100],index_x[0:100],'k--',k13_old[:100],index_x[:100],'k-.')

legend((r'SE Production',r'$He^+$ + $O_2$',r'$H^+$ + $O$',r'$N^+$ + $O_2$',
        r'$N^+$ + $O$',r'$N_2^+$ + $O$'),loc='best',prop={'size': 10})
xlabel(r'Production Rate(cm$^{-3}$$s^{-1}$)',fontsize=14)
ylabel("Altitude(km)",fontsize=14)
yticks(fontsize=14)
xticks(fontsize=14)
axis([10**(-14), 10**3, 0, 2500])
tight_layout()

fname = "CHEMO_Production.eps"
savefig(fname,format='eps')

#Plot Loss
figure(figsize=(6,6),dpi=300)
semilogx(k0[0:100],index_x[0:100],'b-',k1[0:100],index_x[0:100],'b--',k4[:100],index_x[:100],'b-.')
semilogx(k14[0:100],index_x[0:100],'b:',r0[0:100],index_x[0:100],'b-1')
semilogx(k0_old[0:100],index_x[0:100],'k-',k1_old[0:100],index_x[0:100],'k--',k4_old[:100],index_x[:100],'k-.')

legend((r'$O^+$ + $N_2$',r'$O^+$ + $O_2$',r'$O^+$ + $H$',
        r'$O^+$ + $NO$',r'$O^+$ + $e^-$'),loc='best',prop={'size': 10})
xlabel(r'Loss Rate(cm$^{-3}$$s^{-1}$)',fontsize=14)
ylabel("Altitude(km)",fontsize=14)
yticks(fontsize=14)
xticks(fontsize=14)
axis([10**(-14), 10**3, 0, 2500])
tight_layout()

fname = "CHEMO_Loss.eps"
savefig(fname,format='eps')

f1.close()
#f1_s.close()
#f2.close()
