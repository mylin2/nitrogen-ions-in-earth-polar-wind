
from matplotlib.pyplot import *
import linecache
import math


f1 = open("./Figure3_7iPWOM_SEproduction.out","r")
f2 = open("./Figure3_3iPWOM_SEproduction.out","r")


line_num = 0

figure(figsize=(3.5,5),dpi=1000)


for part in range(1):

    for w in range(line_num):
        result_f1=f1.readline()
        result_f2=f2.readline()

    total_photo = np.zeros(390-line_num)
    photo_o = np.zeros(390-line_num)
    photo_n = np.zeros(390-line_num)
    photo_o_old=np.zeros(390-line_num)
    diff=np.zeros(390-line_num)
    o_den=np.zeros(390-line_num)
    photo_no=np.zeros(390-line_num)
    photo_o2=np.zeros(390-line_num)
    photo_n2=np.zeros(390-line_num)
    photo_h = np.zeros(390-line_num)
    photo_he=np.zeros(390-line_num)

    old_photo_no=np.zeros(390-line_num)
    old_photo_o2=np.zeros(390-line_num)
    old_photo_n2=np.zeros(390-line_num)
    old_photo_o = np.zeros(390-line_num)
    old_photo_n = np.zeros(390-line_num)

    original_o=np.zeros(390-line_num)

    #n2=np.zeros(390-line_num)
    index_x = np.linspace(2.2E2, 80.2E2, num=390)

    for j in range(40-line_num):
        result_f1 = f1.readline().split()
        result_f2 = f2.readline().split()

        result_f1[12] = (float(result_f1[12])) #O+
        result_f1[13] = (float(result_f1[13])) #H+
        result_f1[14] = (float(result_f1[14])) #He+
        result_f1[15] = (float(result_f1[15])) #N+
        result_f1[16] = (float(result_f1[16]))
        result_f1[17] = (float(result_f1[17]))
        result_f1[18] = (float(result_f1[18]))
        total_photo[j]=(result_f1[12]+result_f1[13]+result_f1[14]
            +result_f1[15]+result_f1[16]+result_f1[17]+result_f1[18])

        result_f1[12] = math.log10((result_f1[12]))
        result_f1[13] = math.log10((result_f1[13]))
        result_f1[14] = math.log10((result_f1[14]))
        result_f1[15] = math.log10((result_f1[15]))
        result_f1[16] = math.log10((result_f1[16]))
        result_f1[17] = math.log10((result_f1[17]))
        result_f1[18] = math.log10((result_f1[18]))
        result_f2[1] = math.log10(float(result_f2[1]))

        total_photo[j] = math.log10(total_photo[j])
        #total_photo[j] = result_f1[10]+result_f1[11]+result_f1[12]+result_f1[13]+result_f1[14]
        photo_o[j] = result_f1[12]
        photo_h[j]=result_f1[13]
        photo_he[j]=result_f1[14]
        photo_n[j]=result_f1[15]
        photo_n2[j]=result_f1[16]
        photo_no[j]=result_f1[17]
        photo_o2[j]=result_f1[18]
        original_o[j] = result_f2[1]
    plot(original_o[:40],index_x[:40],'b--',total_photo[:40],index_x[:40],'k-',linewidth=3)
    plot(photo_o[:40],index_x[:40],'b-',photo_n[:40],index_x[:40],'C1-',photo_h[:40],index_x[:40],'C8-',photo_he[:40],index_x[:40],'g-',linewidth=3)
    xlabel(r'Log10(Production rate)($cm^{-3}s^{-1}$)',fontsize=12)
    ylabel("Altitude(km)",fontsize=12)
    tick_params(axis='y',labelsize=12)
    tick_params(axis='x',labelsize=12)
    tight_layout()

    fname = "Figure3b.eps"
    savefig(fname,format='eps')

f1.close()
f2.close()
