
from matplotlib.pyplot import *
import linecache
import math

f1 = open("./Figure2_OGOSD.out","r")
old_f1 = open("./Figure2_3iPWOM_ion.out","r")
#get neutral density
old_f2 = open("./Figure2_3iPWOM_neutral.out","r")
#get production Rate
f3 = open("./Figure2_HEproduction.out","r")

line_num = 0

pass_line=390*0
for w in range(pass_line):
    result_f1=f1.readline()

for w in range(5):
    result_f1=f1.readline()
    result_old_f1=old_f1.readline()
    result_old_f2=old_f2.readline()



for part in range(1):

    #Production of He+
    photoHe = np.zeros(390)
    photoHe_old=np.zeros(390)
    photorateHe=np.zeros(390)

    #loss
    k2 = np.zeros(390)
    k3 = np.zeros(390)
    k15=np.zeros(390)

    k2_old = np.zeros(390)
    k3_old = np.zeros(390)
    k15_old=np.zeros(390)

    k2rate=9.7E-10
    k3rate=5.2E-10
    k15rate=7.8E-10

    index_x = np.linspace(2.2E2, 80.2E2, num=390)

    for j in range(390):
        result_f1 = f1.readline().split()
        result_old_f1=old_f1.readline().split()
        result_old_f2=old_f2.readline().split()
        result_f3=f3.readline().split()

        #old pwom
        k2_old[j]=float(result_old_f1[9])*float(result_old_f2[4])*k2rate
        k3_old[j]=float(result_old_f1[9])*float(result_old_f2[5])*k3rate
        k15_old[j]=float(result_old_f1[9])*float(result_old_f2[5])*k15rate
        photoHe_old[j]=3.87E-8*float(result_old_f2[7])
        #put number in array (By reaction)
        photoHe[j]=float(result_f3[14])
        photorateHe[j]=float(result_f3[14])/float(result_old_f2[7])

        k2[j]=float(result_f1[13])*float(result_old_f2[4])*k2rate
        k3[j]=float(result_f1[13])*float(result_old_f2[5])*k3rate
        k15[j]=float(result_f1[13])*float(result_old_f2[5])*k15rate
        #r2[j]=float(result)

figure(figsize=(6,6),dpi=300)
semilogx(photoHe[0:100],index_x[0:100],'g-',photoHe_old[0:100],index_x[0:100],'k-')

legend((r'7iPWOM He$^+$ SE Production',r'3iPWOM He$^+$ SE Production'),loc='best',prop={'size': 10})
xlabel(r'Reaction Rate(cm$^{-3}$$s^{-1}$)',fontsize=14)
ylabel("Altitude(km)",fontsize=14)
yticks(fontsize=14)
xticks(fontsize=14)
axis([10**(-7), 10**1, 0, 2500])
#axis([10**(-15), 10**(-1), 0, 2500])
tight_layout()

fname = "CHEMHE_Production.eps"
savefig(fname,format='eps')

#Plot Loss
figure(figsize=(6,6),dpi=300)
semilogx(k2[0:100],index_x[0:100],'g-',k3[0:100],index_x[0:100],'g--',k15[0:100],index_x[0:100],'g-.')
semilogx(k2_old[0:100],index_x[0:100],'k-',k3_old[0:100],index_x[0:100],'k--',k15_old[0:100],index_x[0:100],'k-.')

legend((r'$He^+$ + $O_2$',r'$He^+$ + $N_2$ (1)',r'$He^+$ + $N_2$ (2)'),loc='best',prop={'size': 10})
xlabel(r'Loss Rate(cm$^{-3}$$s^{-1}$)',fontsize=14)
ylabel("Altitude(km)",fontsize=14)
yticks(fontsize=14)
xticks(fontsize=14)
axis([10**(-7), 10**1, 0, 2500])
#axis([10**(-7), 10**1, 0, 2500])
tight_layout()

fname = "CHEMHE_Loss.eps"
savefig(fname,format='eps')

f1.close()
#f1_s.close()
#f2.close()
