from matplotlib.pyplot import *
import linecache
import math
import numpy

#Declare the data array
line_num = 5
altitude = np.zeros(397-line_num)
OldDensityO = np.zeros(397-line_num)
OldDensityH = np.zeros(397-line_num)
OldDensityHe = np.zeros(397-line_num)

NewDensityO=np.zeros(397-line_num)
NewDensityH=np.zeros(397-line_num)
NewDensityHe = np.zeros(397-line_num)
NewDensityN=np.zeros(397-line_num)
NewDensityN2=np.zeros(397-line_num)
NewDensityTest=np.zeros(397-line_num)
NewDensitye=np.zeros(397-line_num)

#Filename array
filename=["47d","45c","46d","46c","47b","47a","46b","48a"]
figname= ["SolarMaximum_SummerNoon","SolarMaximum_SummerMidnight","SolarMaximum_WinterNoon",
"SolarMaximum_WinterMidnight","SolarMinimum_SummerNoon","SolarMinimum_SummerMidnight",
"SolarMinimum_WinterNoon","SolarMinimum_WinterMidnight"]
simulationame=["OGOSD","OGOSM","OGOWD","OGOWM","AESD","AESM","AEWD","AEWM"]

datapoint=[[74,1,20,39,58],[76,1,20,39,58],[73,1,20,37,55],[73,1,19,37,56],
    [71,1,21,43,63],[79,1,24,43,67],[101,1,26,52,78],[63,1,16,30,48]]

#read data for PWOM result
#calculate mass density between old and new PWOM

for case in range(8):
    result=open("./"+simulationame[case]+".out","r")
    oldresult=open("./old_"+simulationame[case]+".out","r")
    for skipline in range(line_num):
        resultdata=result.readline()
        oldresultdata=oldresult.readline()

    for resultline in range(392):
        resultdata=result.readline().split()
        oldresultdata=oldresult.readline().split()
        for dataperline in range(21):
            resultdata[dataperline] = float(resultdata[dataperline])
            oldresultdata[dataperline] = float(oldresultdata[dataperline])
        altitude[resultline] = resultdata[0]
        #for 7 species, the density index starts at 11
        NewDensityO[resultline] = resultdata[11]
        NewDensityH[resultline] = resultdata[12]
        NewDensityHe[resultline] = resultdata[13]
        NewDensityN[resultline] = resultdata[14]
        OldDensityO[resultline] = oldresultdata[7]
        OldDensityH[resultline] = oldresultdata[8]
        OldDensityHe[resultline] = oldresultdata[9]

    totalline=datapoint[case][0]
    hline=datapoint[case][1]
    nline=datapoint[case][2]
    oline=datapoint[case][3]
    heline=datapoint[case][4]
    shiftData=False
    if(case==0 or case==4 or case==5 or case==7):
        shiftData=True #47 and 48 required to shift
    figure(figsize=(6,6),dpi=300)
    plot(NewDensityO[:50],altitude[:50],'b-',NewDensityN[:50],altitude[:50],'C1-',NewDensityH[:50],altitude[:50],'C8-',linewidth=3)
    plot(NewDensityHe[:50],altitude[:50],'g-',linewidth=3)
    plot(OldDensityO[:50],altitude[:50],'b--',OldDensityH[:50],altitude[:50],'C8--',linewidth=3)
    plot(OldDensityHe[:50],altitude[:50],'g--',linewidth=3)
    csvfile=open("./"+filename[case]+".csv","r")

    density_hplus=np.zeros(nline-hline)
    alt_hplus=np.zeros(nline-hline)
    density_nplus=np.zeros(oline-nline)
    alt_nplus=np.zeros(oline-nline)
    density_oplus=np.zeros(heline-oline)
    alt_oplus=np.zeros(heline-oline)
    density_heplus=np.zeros(totalline+1-heline)
    alt_heplus=np.zeros(totalline+1-heline)

    for i in range(totalline):

        profile=csvfile.readline().split(',')
        #h case
        if (i < nline-hline):
            density_hplus[i]=float(profile[0])
            alt_hplus[i]=float(profile[1])
            if(shiftData):
                density_hplus[i]=float(profile[0])+1
        elif (i >= nline-1 and i <oline-1):
            density_nplus[i-nline+1]=float(profile[0])
            alt_nplus[i-nline+1]=float(profile[1])
            if(shiftData):
                density_nplus[i-nline+1]=float(profile[0])+1
        elif (i >= oline-1 and i <heline-1):
            density_oplus[i-oline+1]=float(profile[0])
            alt_oplus[i-oline+1]=float(profile[1])
            if(shiftData):
                density_oplus[i-oline+1]=float(profile[0])+1
        else:
            density_heplus[i-heline+1]=float(profile[0])
            alt_heplus[i-heline+1]=float(profile[1])
            if(shiftData):
                density_heplus[i-heline+1]=float(profile[0])+1

    plot(density_oplus,alt_oplus,'-ob',density_nplus,alt_nplus,'-oC1',linewidth=3)
    plot(density_hplus,alt_hplus,'-oC8',density_heplus,alt_heplus,'-og',linewidth=3)
            #legend((r'PWOM $O^+$',r'PWOM $N^+$',r'PWOM $H^+$',r'PWOM $He^+$',
            #        dataname[fileorder]+r' $O^+$',dataname[fileorder]+r' $N^+$',
            #        dataname[fileorder]+r' $H^+$',dataname[fileorder]+r' $He^+$'),loc='best',prop={'size': 6})
    tight_layout()
    axis([-3,6,150,1250])
    xlabel(r'Log(n)(cm$^{-3}$)',fontsize=14)
    ylabel('Altitude(km)',fontsize=14)
    tick_params(axis='x',labelsize=14)
    tick_params(axis='y',labelsize=14)

    tight_layout()
    fname=figname[case]+'.eps'
    savefig(fname,format='eps')
    close()
